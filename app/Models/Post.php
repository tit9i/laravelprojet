<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public $table = 'posts';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'user_id',
        'titre',
        'contenu',
        'categorie_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'titre' => 'string',
        'contenu' => 'string',
        'categorie_id' => 'integer',
    ];

    /**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user() {
		return $this->belongsTo(\App\Models\User::class);
	}

    /**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function categorie() {
		return $this->belongsTo(\App\Models\Categorie::class);
	}

    // public function comments()
    // {
    //     return $this->hasMany(\App\Models\Comment::class, 'post_id');
    // }


}
