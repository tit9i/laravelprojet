<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;

    public $table = 'categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'nom',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nom' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    // public static $rules = [
    //     'code' => 'nullable|string|max:10',
    //     'libelle' => 'required|string|max:50',
    //     'description' => 'nullable|string',
    //     'ci' => 'boolean',
    //     'is_visible' => 'boolean',
    //     'is_active' => 'boolean',
    //     'created_by' => 'string|max:255',
    //     'created_at' => 'nullable',
    //     'updated_at' => 'nullable'
    // ];
}
