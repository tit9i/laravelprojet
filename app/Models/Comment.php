<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public $table = 'comments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'contenu',
        'user_id',
        'post_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'contenu' => 'string',
        'user_id' => 'integer',
        'post_id' => 'integer',
    ];

    /**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function user() {
		return $this->belongsTo(\App\Models\User::class);
	}

    /**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 **/
	public function post() {
		return $this->belongsTo(\App\Models\Post::class);
	}
}
