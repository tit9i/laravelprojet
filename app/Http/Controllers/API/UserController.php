<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{
    public function index()
    {
        $retour = [
            "status" => true,
            "description" => "Resultat"
        ];
        return response()->json($retour, 200);
    }

    public function show($id)
    {
        $retour = [
            "status" => true,
            "description" => "Resultat",
            "id" => $id
        ];
        return response()->json($retour, 200);
    }
    

    public function store(Request $request)
    {
        $input = $request->all();
        $password = Hash::make($input['password']); //Pour crypter le mot de passe qui sera enregistré
        $user = User::create([
            'name' => $input['name'],
            'prenom' => $input['prenom'],
            'email' => $input['email'],
            'password' => $password
        ]);
        $retour = [
            "status" => true,
            "data" => $user,
            "message" => "Utilisateur enregistré avec succès"
        ];
        return response()->json($retour, 200);
    }

    public function login(Request $request)
    {
        // Pour appliquer des règles de validation sur les champs
        $validator = Validator::make(
            $request->all(),
            $this->getValidationRules()
        );

        if ($validator->fails())
            return response()->json(["message" => $validator->errors()], 500); // Retour des erreurs

        ///Vérification des identifiants si c'est correcte ou pas.
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return response()->json(["message" => "Identifiant ou mot de passe incorrecte"], 403);
        }

        $user = Auth::user(); //Pour recupérer l'utilisateur dont les identifiants correspondent

        //Création du Token
        $device_name = isset($request->device_name) ? $request->device_name : 'myApp';
        $user->token =  $user->createToken($device_name)->plainTextToken;
        ///////////////////////////
        
        return ["success" => true,
                "data" => $user,
                "message" => 'Connexion effectuée avec succès'];
        // return $this->sendResponse(new UserResource($user), 'Connexion effectuée avec succès');
    }

    private function getValidationRules()
    {
        return [
            'email' => 'required|email|min:5',
            'password' =>'required|min:3',
        ];
    }


    public function update(Request $request, $id)
    {
        
    }

    public function delete($id)
    {
       
    }
}
