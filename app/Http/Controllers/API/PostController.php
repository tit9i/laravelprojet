<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Auth;
use Validator;

class PostController extends Controller
{
    public function index() //Afficher Toutes les données de la table Categorie
    {
        $posts = Post::get(); //Pour recuperer toutes les données de la table
        return PostResource::collection($posts);
    }

    public function show($id)
    {
        $post = Post::where("id", $id)->first();
        
        if(empty($post)){
            return response()->json(["message" => "Ce post n'existe pas"], 200);
        }

        return new PostResource($post);
    }
    

    public function store(Request $request)
    {
        $input = $request->all();

        // Pour appliquer des règles de validation sur les champs
        $validator = Validator::make(
            $request->all(),
            $this->getValidationRules()
        );

        if ($validator->fails())
            return response()->json(["message" => $validator->errors()], 500); // Retour des erreurs

        $user = Auth::user(); //Pour recupérer l'utilisateur dont les identifiants correspondent
        $post = Post::create([
            'titre' => $input['titre'],
            'contenu' => $input['contenu'],
            'user_id' => $user->id,
            'categorie_id' => $input['categorie_id']
        ]);
        return new PostResource($post);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        // Pour appliquer des règles de validation sur les champs
        $validator = Validator::make(
            $request->all(),
            $this->getValidationRules()
        );

        if ($validator->fails())
            return response()->json(["message" => $validator->errors()], 500); // Retour des erreurs

        $post = Post::find($id);
        if(empty($post)){
            return response()->json(["message" => "Cette categorie n'existe pas"], 200);
        }
        $post->titre = $input["titre"];
        $post->contenu = $input["contenu"];
        $post->categorie_id = $input["categorie_id"];
        $post->save();
        return new PostResource($post);
    }

    public function delete($id)
    {
        $post = Post::find($id);
        if(empty($post)){
            return response()->json(["message" => "Ce post n'existe pas"], 200);
        }
        $post->delete();
        $retour = [
            "status" => true,
            "description" => "Suppression effectuée"
        ];
        return response()->json($retour, 200);
    }

    private function getValidationRules()
    {
        return [
            'titre' => 'required|min:5',
            'contenu' =>'required|min:5',
            'categorie_id' =>'required',
        ];
    }
}
