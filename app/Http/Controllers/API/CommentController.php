<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Post;
use App\Http\Resources\CommentResource;
use Illuminate\Support\Facades\Auth;
use Validator;

class CommentController extends Controller
{
    public function index($id) //Afficher Toutes les commentaires d'un post
    {
        $post = Post::find($id);
        if(empty($post)){
            return response()->json(["message" => "Le post que vous voulez commenter n'existe pas"], 200);
        }
        $comments = Comment::where("post_id", $id)->get(); //Pour recuperer les commentaires d'un post
        return CommentResource::collection($comments);
    }

    public function show($id)
    {
        $comment = Comment::where("id", $id)->first();
        
        if(empty($comment)){
            return response()->json(["message" => "Ce commentaire n'existe pas"], 200);
        }

        return new CommentResource($comment);
    }
    

    public function store(Request $request, $id)
    {
        $input = $request->all();

        // Pour appliquer des règles de validation sur les champs
        $validator = Validator::make(
            $request->all(),
            $this->getValidationRules()
        );

        if ($validator->fails())
            return response()->json(["message" => $validator->errors()], 500); // Retour des erreurs

        $post = Post::find($id);
        if(empty($post)){
            return response()->json(["message" => "Le post que vous voulez commenter n'existe pas"], 200);
        }
        $user = Auth::user(); //Pour recupérer l'utilisateur dont les identifiants correspondent
        $comment = Comment::create([
            'contenu' => $input['contenu'],
            'user_id' => $user->id,
            'post_id' => $post->id
        ]);
        return new CommentResource($comment);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        // Pour appliquer des règles de validation sur les champs
        $validator = Validator::make(
            $request->all(),
            $this->getValidationRules()
        );

        if ($validator->fails())
            return response()->json(["message" => $validator->errors()], 500); // Retour des erreurs

        $comment = Comment::find($id);
        if(empty($comment)){
            return response()->json(["message" => "Ce commentaire n'existe pas"], 200);
        }
        $comment->contenu = $input["contenu"];
        $comment->save();
        return new CommentResource($comment);
    }

    public function delete($id)
    {
        $comment = Comment::find($id);
        if(empty($comment)){
            return response()->json(["message" => "Ce commentaire n'existe pas"], 200);
        }
        $comment->delete();
        $retour = [
            "status" => true,
            "description" => "Suppression effectuée"
        ];
        return response()->json($retour, 200);
    }

    private function getValidationRules()
    {
        return [
            'contenu' =>'required|min:5'
        ];
    }
}
