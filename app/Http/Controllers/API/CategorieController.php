<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Http\Resources\CategorieResource;

class CategorieController extends Controller
{

    /**
     * Get Liste Categorie
     * @OA\Get (
     *     path="/api/categories",
     *     tags={"Categorie"},
     *     @OA\Response(
     *         response=200,
     *         description="success",
     *         @OA\JsonContent(
     *              @OA\Property(property="id", type="number", example=1),
     *              @OA\Property(property="nom", type="string", example="categorie 1"),
     *              @OA\Property(property="categorie", type="string", example="ma description")
     *         )
     *     )
     * )
     */
    public function index() //Afficher Toutes les données de la table Categorie
    {
        $categories = Categorie::get(); // SELECT * FROM categories
        // $retour = [
        //     "status" => true,
        //     "data" => $categories
        // ];

        return CategorieResource::collection($categories);
        // return response()->json($retour, 200);
    }

    public function show($id)
    {
        $categorie = Categorie::where("id", $id)->first();
        
        if(empty($categorie)){
            return response()->json(["message" => "Cette categorie n'existe pas"], 200);
        }

        // $retour = [
        //     "status" => true,
        //     "data" => $categorie,
        // ];

        return new CategorieResource($categorie);
        // return response()->json($retour, 200);
    }
    

    public function store(Request $request)
    {
        $input = $request->all();
        $categorie = Categorie::create([
            'nom' => $input['nom'],
            'description' => $input['description']
        ]);
        // $retour = [
        //     "status" => true,
        //     "data" => $categorie,
        //     "message" => "Element crée avec succès"
        // ];
        // return response()->json($retour, 200);
        return new CategorieResource($categorie);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $categorie = Categorie::find($id);
        if(empty($categorie)){
            return response()->json(["message" => "Cette categorie n'existe pas"], 200);
        }
        $categorie->nom = $input["nom"];
        $categorie->description = $input["description"];
        $categorie->save();
        
        // $retour = [
        //     "status" => true,
        //     "data" => $categorie,
        //     "message" => "Element mise à jour avec succès"
        // ];
        return new CategorieResource($categorie);
        // return response()->json($retour, 200);
    }

    public function delete($id)
    {
        $categorie = Categorie::find($id);
        if(empty($categorie)){
            return response()->json(["message" => "Cette categorie n'existe pas"], 200);
        }
        $categorie->delete();
        $retour = [
            "status" => true,
            "description" => "Suppression effectuée"
        ];
        return response()->json($retour, 200);
    }
}
